 <h1>Instructions</h1>
            <ol>
                <li>In order to provide the logical function you must provide both its minterms and variables. 
                    For example if your function is <a href="https://www.codecogs.com/eqnedit.php?latex=\inline&space;\fn_jvn&space;F(A,B,C)=A\bar{B}C&plus;\bar{A}BC&plus;AB\bar{C}&space;\quad" target="_blank"><img src="https://latex.codecogs.com/svg.latex?\inline&space;\fn_jvn&space;F(A,B,C)=A\bar{B}C&plus;\bar{A}BC&plus;AB\bar{C}&space;\quad" title="F(A,B,C)=A\bar{B}C+\bar{A}BC+AB\bar{C} \quad" /></a> than use 3, 5, 6 as your minterms and A, B, C as variables.</li>
                <li>Limit the number of minterms and variables to 16 due to LaTeX support.</li> 
                <li>You can also use symbols like <a href="https://www.codecogs.com/eqnedit.php?latex=\inline&space;\fn_jvn&space;\alpha" target="_blank"><img src="https://latex.codecogs.com/svg.latex?\inline&space;\fn_jvn&space;\alpha" title="\alpha" /></a> and <a href="https://www.codecogs.com/eqnedit.php?latex=\inline&space;\fn_jvn&space;\beta" target="_blank"><img src="https://latex.codecogs.com/svg.latex?\inline&space;\fn_jvn&space;\beta" title="\beta" /></a> by simply using their LaTeX equivalent. For example,</li>   
                <ul>
                    <li>\alpha :<a href="https://www.codecogs.com/eqnedit.php?latex=\inline&space;\fn_jvn&space;\alpha" target="_blank"><img src="https://latex.codecogs.com/svg.latex?\inline&space;\fn_jvn&space;\alpha" title="\alpha" /></a></li>
                    <li>\beta : <a href="https://www.codecogs.com/eqnedit.php?latex=\inline&space;\fn_jvn&space;\beta" target="_blank"><img src="https://latex.codecogs.com/svg.latex?\inline&space;\fn_jvn&space;\beta" title="\beta" /></a></li>
                    <li>\gamma :<a href="https://www.codecogs.com/eqnedit.php?latex=\inline&space;\fn_jvn&space;\gamma" target="_blank"><img src="https://latex.codecogs.com/svg.latex?\inline&space;\fn_jvn&space;\gamma" title="\gamma" /></a></li>
                    <li>\phi:<a href="https://www.codecogs.com/eqnedit.php?latex=\inline&space;\fn_jvn&space;\phi" target="_blank"><img src="https://latex.codecogs.com/svg.latex?\inline&space;\fn_jvn&space;\phi" title="\phi" /></a></li>
                    <li>\psi:<a href="https://www.codecogs.com/eqnedit.php?latex=\inline&space;\fn_jvn&space;\psi" target="_blank"><img src="https://latex.codecogs.com/svg.latex?\inline&space;\fn_jvn&space;\psi" title="\psi" /></a></li>
                </ul>
                <li>Do not use  <code>% , $ </code> and other LaTeX reserved symbols as variables.</li>
            </ol>
